# Headless CMS

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

This repo is used to provide working examples for the Headless CMS spike

## Build & development

Run `gulp` for building and `gulp serve` for preview.
