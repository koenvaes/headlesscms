'use strict';

/**
 * @ngdoc function
 * @name headlessCmsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the headlessCmsApp
 */
angular.module('headlessCmsApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
