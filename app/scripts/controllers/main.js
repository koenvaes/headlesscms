'use strict';

/**
 * @ngdoc function
 * @name headlessCmsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the headlessCmsApp
 */
angular.module('headlessCmsApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
